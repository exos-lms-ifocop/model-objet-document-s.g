const allSpan = document.getElementsByTagName("span");
console.log("🚀 ~ file: exo8.js:2 ~ allSpan:", allSpan);
console.log("🚀 ~ file: exo8.js:2 ~ TYPEOF allSpan:", typeof allSpan);

const allLi = document.getElementsByTagName("li");
console.log("🚀 ~ file: exo8.js:2 ~ allSpan:", allLi);
console.log("🚀 ~ file: exo8.js:2 ~ TYPEOF allSpan:", typeof allLi);

const change = function () {
	console.log(element);
	console.log("le THIS de COLOR", element.style.color);
	console.log("le THIS de COLOR", element.style.fontSize);
};

function changElement() {
	//allLi[0].style.color = "green";
	this.style.color = "red";

	console.log(this.style);
	//	console.log(typeof this);
	//
	//	console.log("le THIS de COLOR", this.style.color);
	//	console.log("le fontSize de COLOR", this.style.fontSize);
}

for (let i = 0; allLi[i]; i++) {
	console.log("🚀 ~ file: exo8.js:9 ~ i:", i);
	console.log("🚀 ~ file: exo8.js:2 ~ allSpan:", allLi[i]);
	allLi[i].addEventListener("click", change);
}

for (let i = 0; allLi[i]; i++) {
	console.log("🚀 ~ file: exo8.js:9 ~ i:", i);
	console.log("🚀 ~ file: exo8.js:2 ~ allSpan:", allLi[i]);
	allLi[i].addEventListener("click", changElement);
}

//allLi[0].addEventListener("click", changElement);

//allLi[0].addEventListener("click", change);

//
//() => {style.color = "green";}
//
//

//allLi[0].style.color = "green";

for (const i of allSpan) {
	console.log("🚀 ~ file: exo8.js:50 ~ i:", i);

	console.log("🚀 ~ SOUCI ???? ~ allSpan:", allSpan[i]);
}

//Objets iterables

//for of

const monTitre1 = document.getElementsByTagName("h1");
console.log("🚀 ~ file: exo8.js:16 ~ monTitre1:", monTitre1);
console.log("🚀 ~ file: exo8.js:16 ~ monTitre1:", typeof monTitre1);

//

for (i of monTitre1) {
	console.log("🚀 ~ file: exo8.js:16 ~ monTitre1:", monTitre1);
}
