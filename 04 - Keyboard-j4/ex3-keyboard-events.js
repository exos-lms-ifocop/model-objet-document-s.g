window.addEventListener("load", function () {
  const mario = document.getElementById("mario");
  mario.style.left = 0;
  mario.style.top = 0;

  let arrowRightDown = false;

  const moveRight = function () {
    if (arrowRightDown) {
      console.log("moveRight");
      const posLeft = parseFloat(mario.style.left);
      mario.style.left = posLeft + 10 + "px";
      console.log(posLeft);
      moveRight();
    }
  };

  window.addEventListener("keydown", function (monEvenementTouche) {
    switch (monEvenementTouche.key) {
      case "ArrowUp":
        console.log("HAUT");
        break;
      case "ArrowDown":
        console.log("BAS");
        break;
      case "ArrowRight":
        if (!arrowRightDown) {
          arrowRightDown = true;
          moveRight();
        }
        console.log("DROITE");
        // const posLeft = parseFloat(mario.style.left);
        // console.log(posLeft);
        // mario.style.left = posLeft + 10 + "px";
        break;
      case "ArrowLeft":
        console.log("GAUCHE");
        break;
    }
  });

  window.addEventListener("keyup", function (monEvenementTouche) {
    switch (monEvenementTouche.key) {
      case "ArrowUp":
        console.log("HAUT");
        break;
      case "ArrowDown":
        console.log("BAS");
        break;
      case "ArrowRight":
        console.log("DROITE");
        arrowRightDown = false;
        break;
      case "ArrowLeft":
        console.log("GAUCHE");
        break;
    }
  });
});
