const monObj = {
	name: "I AM WHO I AM",
	metho1: function () {
		console.log("je suis 1");
		console.log("Je suis le 1/THIS", this);

		return this;
	},
	metho2: function () {
		console.log("je suis 2-2");
		console.log("Je suis le 2/THIS", this);

		return this;
	},
	metho3: function () {
		console.log("je suis une 3-3-3");
		console.log("Je suis le 3/THIS", this);

		return this;
	},
};

const myFirst = monObj;
console.log("🚀 ~ myFirst:", myFirst);
const m1 = monObj.metho1();
const m2 = m1.metho2();
const m3 = function () {
	m2.metho3();
};

m3();
//
//const m2 = monObj.metho2();
//const m3 = monObj.metho3();
//m1.metho1.metho2.metho3();
//
console.log(m1);
