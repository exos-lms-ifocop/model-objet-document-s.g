const myChaton = document.getElementById("chaton");
const mesTitres = document.querySelector("h1");
console.log("🚀 ~ file: exo21.js:2 ~ myChaton:", myChaton);
console.log("🚀 ~ file: exo21.js:2 ~ myChaton:", typeof myChaton);

console.log("STYLE in PROTOTYPE ?", myChaton.hasOwnProperty("style"));
//

const moveDiv = function (e) {
	let monX = e.clientX;
	console.log("🚀 ~ monX:", monX);
	let monY = e.clientY;
	//console.log("🚀 ~ file: exo21.js:7 ~ moveDiv ~ monY:", monY);
	mesTitres.innerText = `    Client X/Y: ${monX}, ${monY}`;
	//
	console.log(
		"🚀 ~ file: exo21.js:17 ~ moveDiv ~ myChaton.style.top:",
		myChaton.style.position.top
	);
	//

	myChaton.style.top = monY + "px";
	myChaton.style.left = monX + "px";
};
//
document.addEventListener("mousemove", moveDiv);

//

//
//function logCuror(e) {
//	myChaton.innerText = `
//    Screen X/Y: ${e.screenX}, ${e.screenY}
//    Client X/Y: ${e.clientX}, ${e.clientY}`;
//}

//    Screen X/Y: ${e.screenX}, ${e.screenY}
