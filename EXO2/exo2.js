//alert(window.navigator.userAgent);
//

const idChromeAgend =
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36";

const idFireFoxAgent =
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:121.0) Gecko/20100101 Firefox/121.0";

const idOperaAgent =
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 OPR/106.0.0.0";

const idEdgeAgent =
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Edg/120.0.0.0";
//
const userAgent = window.navigator.userAgent;
console.log(userAgent);

console.log(typeof userAgent);
//const isChrome = userAgent.indexOf("Chrome") > -1;

const whoIsIt = userAgent.split(" ");
console.log("🚀 ~ file: exo2.js:9 ~ whoIsIt:", whoIsIt);

const whoIsIt_L = whoIsIt.length;
console.log("🚀 ~ file: exo2.js:12 ~ whoIsIt_L:", whoIsIt_L);

const testUserAgent = 0;

const testMyAgent = whoIsIt[whoIsIt_L - 1];

console.log("🚀 ~ file: exo2.js:15 ~ testMyAgent:", testMyAgent);

//userAgent.includes("Edg");
//userAgent.includes("OPR");
//userAgent.includes("Firefox");
//userAgent.includes("Chrome");

console.log(userAgent.includes("Chrome"));
console.log(userAgent.includes("OPR"));
console.log(userAgent.includes("Firefox"));
console.log(userAgent.includes("Edg"));

if (testMyAgent === "Edg/120.0.0.0") {
	console.log("🚀 ~ votre navigateur est EDGE");
	alert("🚀 ~ votre navigateur est EDGE");
	open("https://www.microsoft.com/fr-fr/edge");
} else if (testMyAgent === "OPR/106.0.0.0") {
	console.log("🚀 ~ votre navigateur est OPERA");
	alert("🚀 ~ votre navigateur est OPERA");
	open("https://www.opera.com");
} else if (testMyAgent === "Firefox/121.0") {
	console.log("🚀 ~ votre navigateur est FIREFOX");
	alert("🚀 ~ votre navigateur est FIREFOX");
	open("https://www.mozilla.org/fr/firefox/new/");
} else if (testMyAgent === "Safari/537.36") {
	console.log("🚀 ~ votre navigateur est CHROME");
	alert("🚀 ~ votre navigateur est CHROME");
	open("https://www.google.com/chrome/");
} else if (testMyAgent === "Safari/605.1.15") {
	console.log("🚀 ~ votre navigateur est SAFARI");
	alert("🚀 ~ votre navigateur est SAFARI");
	open("https://www.apple.com/fr/safari/");
}
